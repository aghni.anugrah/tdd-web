"""story6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from form import views
from story9 import views as views9
from story9.views import logout_view
from django.contrib.auth import views as viewsauth

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.form),
    re_path('story-6/', views.form),
    re_path('fill_form', views.fill_form, name='fill_form'),
    re_path('view-status', views.table),
    re_path('profile', views.profile),
    re_path('cv', views.accordion, name="home"),
    path('story-9/', views9.index, name="book"),
    path('data', views9.data, name='data'),
    re_path('subscribe', views9.subscribe, name='subscribe'),
    re_path('success', views9.success, name='success'),
    re_path('validate', views9.validate),
    path('sub-table', views9.getSubList, name='get-sub-list'),
    path('table', views9.table, name='table'),
    path('delete-this', views9.deleteThis, name='delete-this'),
    path('login', views9.login, name="login"),
    path('login/', viewsauth.LoginView.as_view(), name="login"),
    path('logout/', logout_view , name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('add/', views9.addFav, name='add'),
    path('remove/', views9.removeFav, name='remove'),
]
