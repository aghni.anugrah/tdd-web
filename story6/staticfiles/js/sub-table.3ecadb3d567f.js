$(document).ready(function() {
    $.ajax({
        url: "/sub-table",
        datatype: 'story9.models.Subscriber',
        success: function(data) {
            var result = '<tr>';
            for (var i = 0; i < data.items.length; i++) {
                result += "<td class='align-middle'>" + data.items[i].name + "</td>" +
                "<td class='align-middle'>" + data.items[i].email + "</td></tr>";
            }
            $("tbody").append(result)
        }
        error: function() {}
    })
});