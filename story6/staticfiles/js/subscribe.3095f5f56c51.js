$(document).ready(function() {
    var name = "";
    var password = "";
    $("#id_name").change(function() {
        name = $(this).val();
    });

    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                console.log(password.length)
                if(data.not_valid) {
                    alert("This email has subscribed this channel");
                }
//                else {
//                    $("#submit").prop("disabled", false);
//                }
            }
        });
    });
    $("#id_password").change(function() {
        password = $(this).val();
        if (password.length > 3) {
            $("#submit").prop("disabled", false);
        }
    })
    $("#submit").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/success",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "hi " + data.name,
                    text: 'thanks for subbing. now go away...',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    }).then(function() {
                        window.location = "/"
                    });
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'whoops...',
                    text: 'something went wrong, bud',
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});
