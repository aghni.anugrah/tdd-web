from django.db import models

# Create your models here.
class Subscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)

    def asDict(self):
        return {
            'name' : self.name,
            'email' : self.email,
            'password' : self.password,
        }