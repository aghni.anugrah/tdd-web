from django.contrib import admin

# Register your models here.
from story9.models import Subscriber

admin.site.register(Subscriber)