from django.urls import path
from story9 import views
from story9.views import library, addFav, removeFav

urlpatterns = [
    path('cv', views.home, name = "home"),
    path('data', views.data, name='data'),
    path('', views.subscribe, name='subscribe'),
	path('', views.success, name='success'),
	path('', views.validate),
    path('', views.getSubList),
    path('', views.table),
    path('', views.deleteThis),
    path('', views.login),
	path('add/', addFav, name="add"),
	path('remove/', removeFav, name="remove"),

]