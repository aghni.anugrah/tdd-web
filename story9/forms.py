from django import forms

class SubscribeForm(forms.Form):
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'your full name here',
    }

    email_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'make sure that it is valid'
    }

    password_attrs = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'dont let anyone knows about this'
    }

    name = forms.CharField(widget=forms.TextInput(attrs=name_attrs), error_messages={"required": "Enter your name"})
    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs), error_messages={"required": "Enter your email"})
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs), error_messages={"required": "Enter your password"})
