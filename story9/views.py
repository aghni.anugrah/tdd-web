import requests
from django.core import serializers
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render

# Create your views here.
import json

from story9.forms import SubscribeForm
from story9.models import Subscriber
from django.views.decorators.csrf import csrf_exempt

response = {}

def index(request):
    print("masuk sih")
    if request.user.is_authenticated:
        print("nop")
        request.session['user'] = request.user.username
        request.session['email'] = request.user.email
        count = request.session.get('count', 0)
        print(dict(request.session))
        for (key, value) in request.session.items():
            print('{} => {}'.format(key, value))
    print("ahsda")
    return render(request, 'num.html', response)

def login(request):
    return render(request, 'login.html', response)

def data(request):
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    return JsonResponse(json_read)

def home(request):
    response = {}
    return render(request, 'cv.html', response)

def subscribe(request):
    response = {}
    response['form'] = SubscribeForm()
    return render(request, 'subs.html', response)

@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()}
    return JsonResponse(data)

def success(request):
    form = SubscribeForm(request.POST or None)
    data = {}
    if (form.is_valid()):
        data = form.cleaned_data
        newsubscriber = Subscriber(name=data['name'], password=data['password'], email=data['email'])
        newsubscriber.save()
        data = {'name': data['name']}
    elif (request.method == "GET"):
        obj = Subscriber.objects.all()
        data = serializers.serialize('json', obj)
    return JsonResponse(data, safe=False)

def getSubList(request):
    allSubber = Subscriber.objects.all()
    allSubber = [obj.asDict() for obj in allSubber]
    return JsonResponse({'subs' : allSubber}, content_type='applications/json')

def table(request):
    return render(request, 'table.html', response)

@csrf_exempt
def deleteThis(request):
    if request.method == 'POST':
        print("masuk")
        subId = request.POST['pk']
        Subscriber.objects.filter(pk=subId).delete()
        return HttpResponse(json.dumps({'message': 'Succeed'}),
                            content_type='application/json')
    else:
        return HttpResponse(json.dumps({'message': "There's something wrong. Try again."
                            }), content_type='application/json')

def addFav(request):
    print(dict(request.session))
    request.session['count'] = request.session['count'] + 1
    return HttpResponse(request.session['count'], content_type='application/json')


def removeFav(request):
    request.session['count'] = request.session['count'] - 1
    return HttpResponse(request.session['count'], content_type='application/json')


def logout_view(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/story-9')
