$(document).ready(function () {
	$.ajax({
		url: "/data",
		datatype: 'json',
		success: function (data) {
			var result = '<tr>';
			for (var i = 0; i < data.items.length; i++) {
				result += "<th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
					"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" +
					"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var count=0;
function favorite(id) {
	var star = $('#' + id).html();
	if(star.includes("gray")) {
        $.ajax({
    		url: "/add/",
    		dataType: 'json',
    		success: function(result) {
				var count = JSON.parse(result);
				console.log(result)
		        $('#' + id).html("<span style='color: #F9F871;' class='fa fa-star'></span>");
		        $('#fav').html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count + " ");
			}
		});
	}
    else{
        $.ajax({
        	url: "/remove/",
    		dataType: 'json',
    		success: function(result) {
				var count = JSON.parse(result);
				console.log(result)
		        $('#'+id).html("<span style='color: gray;' class='fa fa-star'></span>");
		        $("#fav").html("<span style='color: #F9F871;' class='fa fa-star'></span>" + count + " ");
			}
   		 });
	}
}
