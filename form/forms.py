from django import forms

class Status(forms.Form):
    attrs = {
        'class':'form-control'
    }

    status = forms.CharField(label = "status", required = True, widget = forms.TextInput(attrs = attrs))