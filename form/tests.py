from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.utils.encoding import force_text
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from story9.models import Subscriber
from .views import form
from .models import Status
from django.urls import resolve

# Create your tests here.
class story6Test(TestCase):

	def test_request_story6(self):
		response = Client().get('/story-6/')
		self.assertEqual(response.status_code, 200)

	def test_template_story6(self):
		response = Client().get('/story-6/')
		self.assertTemplateUsed(response, 'index.html')

	# def test_func_story6(self):
	# 	found = resolve('/story-6')
	# 	self.assertEqual(found.func, form)

	def test_create_object_model(self):
		new_status = Status.objects.create(status='Saya baik-baik saja')
		counting_object_status = Status.objects.all().count()
		self.assertEqual(counting_object_status, 1)

	def test_profile_has_hello(self):
		response = Client().get('/profile')
		utf = response.content.decode('utf-8')
		self.assertIn("Born and raised in Jakarta", utf)

	def test_blank_page(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_request_story10(self):
		response = Client().get('/subscribe/')
		self.assertEqual(response.status_code, 200)

	def test_template_story10(self):
		response = Client().get('/subscribe/')
		self.assertTemplateUsed(response, 'subs.html')

	def test_create_object_model_subs(self):
		new_status = Subscriber.objects.create(name="test", email="test@test.com", password="testtesttest")
		counting_object_status = Subscriber.objects.all().count()
		self.assertEqual(counting_object_status, 1)

	def test_double_subscriber(self):
		nama = 'rae'
		password = 'bhasdhbjsahjsabd'
		email = 'rae@gmail.com'
		go = Client().post('/validate', {'email': email})
		self.assertJSONEqual(
			force_text(go.content),
			{'not_valid': False}
		)
		Client().post('/success', {'name': nama, 'password': password, 'email': email})
		subsum = Subscriber.objects.all().count()
		self.assertEqual(subsum, 1)
		go = Client().post('/validate', {'email': email})
		self.assertJSONEqual(
			str(go.content, encoding='utf8'),
			{'not_valid': True}
		)
		subsum = Subscriber.objects.all().count()
		self.assertEqual(subsum, 1)

# class Story7FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Story7FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story7FunctionalTest, self).tearDown()
#
#     def test_input_todo(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + "/story-6/")
#         selenium.implicitly_wait(20)
#         input = selenium.find_element_by_name('status')
#         submit = selenium.find_element_by_id('submitbutton')
#         # Fill the form with data
#         # input.send_keys('Coba Coba')
#         # submitting the form
#         # submit.send_keys(Keys.RETURN)
#         # submit.click()
#         # selenium.get(self.live_server_url + "/view-status/")
#         # coba = selenium.find_element_by_tag_name('p').text
#         # self.assertEqual("Coba Coba", coba)
#
#     def test_position(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + "/story-6/")
#         title = selenium.find_element_by_name("judul")
#         button = selenium.find_element_by_id("submitbutton")
#         self.assertEqual(title.location, {'x': 175, 'y': 76})
#         self.assertEqual(button.location, {'x': 175, 'y': 344})
#
#     def test_css(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + "/story-6/")
#         title = selenium.find_element_by_name("judul")
#         button = selenium.find_element_by_id("submitbutton")
#         self.assertEqual(title.value_of_css_property("text-align"), "left")
#         self.assertEqual(button.value_of_css_property("margin"), "0px")
