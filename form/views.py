from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Status
from .models import Status as myStatus

# Create your views here.
response = {}

def index(request):
    return render(request, 'index.html', response)

def form(request):
    response['fill_form'] = Status
    return render(request, 'index.html', {'form' : Status})

def fill_form(request):
    form = Status(request.POST or None)
    if (request.method == 'POST'):
        response['status'] = request.POST['status']
        jadwal = myStatus()
        jadwal.status = request.POST['status']
        jadwal.save()
        return HttpResponseRedirect('/view-status')
    else:
        return HttpResponseRedirect('/')

def table(request):
    status = myStatus.objects.all()
    response['table'] = status
    return render(request, 'status.html', response)

def profile(request):
    return render(request, 'profile.html', response)

def accordion(request):
    return render(request, 'cv.html', response)