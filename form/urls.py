from django.urls import path
from . import views

urlpatterns = [
	path('', views.form),
	path('', views.fill_form, name='fill_form'),
	path('', views.table),
	path('', views.profile),
	path('', views.accordion, name='cv')
]
